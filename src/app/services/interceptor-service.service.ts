import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private _auth: AuthService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const idToken = localStorage.getItem('token');
    console.log(idToken);
    if (idToken) {
      const cloned = req.clone({
        headers: req.headers.set('Bearer', idToken),
      });
      console.log('cloned ' + JSON.stringify(cloned.headers));
      return next.handle(cloned);
    } else {
      return next.handle(req);
    }
  }
}

//   intercept(
//     request: HttpRequest<any>,
//     next: HttpHandler
//   ): Observable<HttpEvent<any>> {
//     if (!request.headers.has('Content-Type')) {
//       request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
//     }
//     request = request.clone({ headers: request.headers.set('Accept', 'application/json') }).clone({
//       setHeaders: {
//         Authorization: `${this._auth.getToken()}`
//       }
//     });

//     return next.handle(request);
//   }
// }
